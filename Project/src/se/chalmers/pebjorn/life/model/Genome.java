package se.chalmers.pebjorn.life.model;
import java.awt.Color;
import java.util.Random;


/**
 * A class that represents a genome.
 * @author Plankton555
 * @version (2012-02-19)
 */
public class Genome {

	/** The amount of color change. */
	private final int COLOR_CHANGE = 13;
	/** The probability of mutating the color. */
	private final double COLOR_MUT_FACTOR = 0.4;
	/** The probability of mutating an integer. */
	private final double INT_MUT_FACTOR = 0.1;
	/** The probability of mutating a double. */
	private final double DOUBLE_MUT_FACTOR = 0.1;
	/** The generation number. */
	private final int GENERATION;
	
	
	private final Color color;
	
	private final int spawnSize;
	private final int parentSize;
	private final int starveSize;
	
	private final int averageExcrementTime;
	private final int excrTimeSpread;
	
	private final double speedChange;
	private final double maxSpeed;
	private final int rangeOfSight;
	
	/**
	 * Creates a new genome with the given color and the default values.
	 * @param color The color of the genome.
	 */
	public Genome(Color color)
	{
		this.GENERATION = 1;
		
		Random rnd = LifeSimulation.rand;
		this.color = color;
		this.spawnSize = (int)(rnd.nextFloat()*5 + 8); // interval 8 - 12
		this.parentSize = (int)(rnd.nextFloat()*8 + 16); // interval 16 - 23
		this.starveSize = (int)(rnd.nextFloat()*3 + 4); // interval 4 - 6
		this.excrTimeSpread = 300;
		this.speedChange = rnd.nextDouble()*0.4 + 0.3; // interval 0.3 - 0.7
		this.maxSpeed = rnd.nextDouble()*0.2 + 0.9; // interval 0.9 - 1.1
		this.rangeOfSight = (int)(rnd.nextFloat()*16 + 40); // interval 40 - 55
		this.averageExcrementTime = metabolismRate();
		
		// Random start attributes (seems to make the game freeze sometimes...)
		
		/*
		this.color = color;
		this.spawnSize = 10;
		this.parentSize = 20;
		this.starveSize = 6;
		this.excrTimeSpread = 300;
		this.speedChange = 0.5;
		this.maxSpeed = 1;
		this.rangeOfSight = 50;
		this.averageExcrementTime = metabolismRate();
		*/
		
	}
	
	/**
	 * Generates a new, maybe mutated, genome from another genome.
	 * @param parent The parent genome.
	 */
	private Genome(Genome parent)
	{
		this.GENERATION = parent.getGeneration()+1;
		
		this.color = mutateColor(parent.color);
		this.spawnSize = mutateInt(parent.spawnSize);
		this.parentSize = mutateInt(parent.parentSize);
		this.starveSize = mutateInt(parent.starveSize);
		this.excrTimeSpread = (int)mutateDouble(parent.excrTimeSpread);
		this.speedChange = mutateDouble(parent.speedChange);
		this.maxSpeed = mutateDouble(parent.maxSpeed);
		this.rangeOfSight = (int)mutateDouble(parent.rangeOfSight);
		//this.averageExcrementTime = (int)mutateDouble(parent.averageExcrementTime);
		this.averageExcrementTime = metabolismRate();
	}
	
	/**
	 * Returns a new, maybe mutated, genome.
	 * @return An offspring genome.
	 */
	public Genome getOffspring()
	{
		return new Genome(this);
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return the newSize
	 */
	public int getNewSize() {
		return spawnSize;
	}

	/**
	 * @return the spawnSize
	 */
	public int getSpawnSize() {
		return parentSize;
	}

	/**
	 * @return the starveSize
	 */
	public int getStarveSize() {
		return starveSize;
	}

	/**
	 * @return the averageExcrementTime
	 */
	public int getAverageExcrementTime() {
		return averageExcrementTime;
	}

	/**
	 * @return the excrTimeSpread
	 */
	public int getExcrTimeSpread() {
		return excrTimeSpread;
	}

	/**
	 * @return the speedChange
	 */
	public double getSpeedChange() {
		return speedChange;
	}

	/**
	 * @return the maxSpeed
	 */
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	/**
	 * @return the rangeOfSight
	 */
	public int getSightRange() {
		return rangeOfSight;
	}
	
	/**
	 * @return The generation.
	 */
	public int getGeneration()
	{
		return GENERATION;
	}
	
	/**
	 * Mutates a color.
	 * @param color The original color.
	 * @return The resulting color.
	 */
	private Color mutateColor(Color color)
	{
		if (Math.random() < COLOR_MUT_FACTOR)
		{
			int change = Math.random()<0.5 ? COLOR_CHANGE : -COLOR_CHANGE;
			
			int red = color.getRed();
			int green = color.getGreen();
			int blue = color.getBlue();
			
			switch ((int)(Math.random()*3)) {
			case 0:
				red = (int)Utils.clamp(red+change, 0, 255);
				break;
			case 1:
				green = (int)Utils.clamp(green+change, 0, 255);
				break;
			case 2:
				blue = (int)Utils.clamp(blue+change, 0, 255);
				break;

			default:
				break;
			}
			return new Color(red, green, blue);
			
		}
		else
		{
			return color;
		}
	}
	
	/**
	 * Mutates an integer.
	 * @param n The original integer.
	 * @return The resulting integer.
	 */
	private int mutateInt(int n)
	{
		if (Math.random() < INT_MUT_FACTOR)
		{
			int output = n;
			switch ((int)(Math.random()*2)) {
			case 0:
				output = Math.max(3, n+1); //Can't be lower than 3
				break;
			case 1:
				output = Math.max(3, n-1);
				break;

			default:
				break;
			}
			
			return output;
		}
		else
		{
			return n;
		}
	}
	
	/**
	 * Mutates a double.
	 * @param n The original double.
	 * @return The resulting double.
	 */
	private double mutateDouble(double n)
	{
		if (Math.random() < DOUBLE_MUT_FACTOR)
		{
			double output = n;
			switch ((int)(Math.random()*2)) {
			case 0:
				output = n*1.1;
				break;
			case 1:
				output = n*0.9;
				break;

			default:
				break;
			}
			
			return output;
		}
		else
		{
			return n;
		}
	}
	
	/**
	 * Returns a text string of the information in this genome.
	 */
	public String toString()
	{
		return "Generation:\t" + GENERATION + 
				"\nColor:\tR=" + color.getRed() + " G=" + color.getGreen() + " B=" + color.getBlue() + 
				"\nSight range:\t" + rangeOfSight +
				"\nSpawn size:\t" + spawnSize + 
				"\nParent size:\t" + parentSize +
				"\nStarve size:\t" + starveSize +
				"\nMetabolism:\t" + averageExcrementTime +
				"\nEnergy spread:\t" + excrTimeSpread +
				"\nSpeed change:\t" + speedChange +
				"\nMax speed:\t" + maxSpeed;
	}
	
	private int metabolismRate()
	{
		return (int)(12250/rangeOfSight + 52.5/speedChange + 350/maxSpeed);
	}
}
