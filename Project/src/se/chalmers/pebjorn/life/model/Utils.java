package se.chalmers.pebjorn.life.model;

import java.awt.Color;

import javax.vecmath.Vector2d;

/**
 * A utility class.
 * @author Plankton555
 * @version (2012-04-15)
 */
public class Utils {

	/**
	 * Restricts a value to be within a specified range.
	 * @param value The value to clamp.
	 * @param min The minimum value. If value is less than min, min will be returned.
	 * @param max The maximum value. If value is greater than max, max will be returned.
	 * @return If value > max, max will be returned. If value < min, min will be returned.
	 * If min <= value >= max, value will be returned.
	 */
	public static double clamp(double value, double min, double max) {
		return (value > max ? max : (value < min ? min : value));
	}
	
	public static Vector2d addVectors(Vector2d v1, Vector2d v2)
	{
		Vector2d output = new Vector2d();
		output.add(v1, v2);
		return output;
	}
	
	public static Vector2d subVectors(Vector2d v1, Vector2d v2)
	{
		Vector2d output = new Vector2d();
		output.sub(v1, v2);
		return output;
	}
	
	public static Vector2d scaleVector(Vector2d v, double d)
	{
		Vector2d output = new Vector2d();
		output.scale(d, v);
		return output;
	}
	
	public static double distance(Vector2d v1, Vector2d v2)
	{
		Vector2d output = new Vector2d();
		output.sub(v1, v2);
		return output.length();
	}
	
	/**
	 * Determines whether the colors are almost the same.
	 * @param c1 Source parameter.
	 * @param c2 Source parameter.
	 * @return true if the colors doesn't differ much, else false.
	 */
	public static boolean almostSameColor(Color c1, Color c2)
	{
		int diff = 20;
		if (c1.equals(c2))
		{
			return true;
		}
		else
		{
			// if the sum of the color differences is more than variable diff
			return (Math.abs(c1.getRed()-c2.getRed()) +
					Math.abs(c1.getGreen()-c2.getGreen()) +
					Math.abs(c1.getBlue()-c2.getBlue()) <= diff);
		}
	}
}
