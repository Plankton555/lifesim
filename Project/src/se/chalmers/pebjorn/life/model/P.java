package se.chalmers.pebjorn.life.model;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Contains parameters and help methods for Life.
 * @author Plankton555
 * @version (2012-02-16)
 */
public class P {

	/** The view dimension. */
	public static final Dimension DIMENSION = new Dimension(
			(int)Toolkit.getDefaultToolkit().getScreenSize().getWidth()-400,
			(int)Toolkit.getDefaultToolkit().getScreenSize().getHeight()-200);
	
	public static final int MS_PER_FRAME = 16;
	public static boolean debug = false;
}
