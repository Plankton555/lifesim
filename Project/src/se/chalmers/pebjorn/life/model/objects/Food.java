package se.chalmers.pebjorn.life.model.objects;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.vecmath.Vector2d;


/**
 * A class representing a piece of food.
 * @author Plankton555
 * @version (2011-12-21)
 */
public class Food extends GameObject {

	/** The maximum initial speed of a food piece. */
	private final double MAX_SPEED = 3;
	/** The rate at which the food piece will slow down. */
	private final double FRICTION = 0.9;
	
	/**
	 * Creates a new piece of food.
	 * @param position The position of the food.
	 * @param color The color of the food.
	 */
	public Food(Vector2d position, Color color)
	{
		super(position);
		this.color = color;
		this.size = 4;
		this.speed = new Vector2d(
				Math.random()*MAX_SPEED - MAX_SPEED/2,
				Math.random()*MAX_SPEED - MAX_SPEED/2);
	}
	
	/**
	 * Updates the food by one step.
	 * @param clientBounds The dimension of the view.
	 */
	public void update(Dimension clientBounds)
	{
		position.add(speed);
		
		bounceOnWalls(clientBounds);
		keepInBounds(clientBounds);
		
		speed.scale(FRICTION);
		if (speed.length() < 0.01)
		{
			speed = new Vector2d(0, 0);
		}
	}
	
	/**
	 * Draws the food.
	 * @param g The graphics object.
	 */
	public void draw(Graphics g)
	{
		g.setColor(color);
		g.fillRect((int)(position.x-size/2), (int)(position.y-size/2), size, size);
	}
}
