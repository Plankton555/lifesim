package se.chalmers.pebjorn.life.model.objects;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;

import javax.vecmath.Vector2d;

import se.chalmers.pebjorn.life.model.Genome;
import se.chalmers.pebjorn.life.model.LifeSimulation;
import se.chalmers.pebjorn.life.model.P;
import se.chalmers.pebjorn.life.model.Utils;

/**
 * A class representing an amoeba.
 * @author Plankton555
 * @version (2012-02-19)
 */
public class Amoeba extends GameObject {

	/** The size of a "newborn" amoeba. */
	private final int NEWSIZE;
	/** The size at which an amoeba will spawn. */
	private final int SPAWNSIZE;
	/** The size at which an amoeba will starve to death. */
	private final int STARVESIZE;
	/** Average time interval at which an amoeba will excrement. */
	private final int AVERAGE_EXCREMENT;
	/** The time spread from the excrement interval. */
	private final int EXCR_SPREAD;
	
	/** The maximum amount of speed an amoeba can change during one cycle. */
	private final double SPEED_CHANGE;
	/** The maximum speed an amoeba can move in. */
	private final double MAX_SPEED;
	/** The range at which the amoeba can detect things. */
	private final int RANGE_OF_SIGHT;
	
	/** The genome of the amoeba. */
	private Genome genome;
	
	/** Amount of time that's passed since the amoeba's last excremention. */
	private int timeSinceLastExcrement = 0;
	/** The time when the amoeba will excrement the next time. */
	private int nextExcrement;
	
	// TODO Clean up code
	
	/** Moves towards this target if moveToTarget() is called. */
	private Vector2d target;
	
	private int lastStateUpdate = 0;
	private int updateStateInterval = 20;
	private enum State
	{
		Roam, Eat;
	}
	private State state;
	
	/** The running simulation. */
	private LifeSimulation simulation;
	
	/**
	 * Creates an amoeba.
	 * @param position The position where the amoeba will spawn.
	 * @param genome The amoeba's genome.
	 * @param simulation The running simulation.
	 */
	public Amoeba(Vector2d position, Genome genome, LifeSimulation simulation)
	{
		super(position);
		this.genome = genome;
		this.color = genome.getColor();
		this.NEWSIZE = genome.getNewSize();
		this.SPAWNSIZE = genome.getSpawnSize();
		this.STARVESIZE = genome.getStarveSize();
		this.AVERAGE_EXCREMENT = genome.getAverageExcrementTime();
		this.EXCR_SPREAD = genome.getExcrTimeSpread();
		this.SPEED_CHANGE = genome.getSpeedChange();
		this.MAX_SPEED = genome.getMaxSpeed();
		this.RANGE_OF_SIGHT = genome.getSightRange();
		
		this.size = NEWSIZE;
		this.nextExcrement = AVERAGE_EXCREMENT;
		this.speed = new Vector2d(1, 1);
		this.state = State.Roam;
		this.simulation = simulation;
	}
	
	/**
	 * Upates the amoeba one cycle.
	 * @param clientBounds The dimension of the view.
	 */
	public void update(Dimension clientBounds, List<Food> closeFood)
	{
		// TODO Allow attacking other organisms in some way.
		updateState(closeFood);
		move();
		
		position.add(speed);
		bounceOnWalls(clientBounds);
		keepInBounds(clientBounds);
		
		leaveExcrement();
		
		checkIfDead();
	}

	/**
	 * Draws the amoeba.
	 * @param g The graphics object.
	 */
	public void draw(Graphics g)
	{
		if (P.debug)
		{
			g.setColor(Color.BLACK);
			//Draw sight line
			g.drawOval((int)(position.x-RANGE_OF_SIGHT),
					(int)(position.y-RANGE_OF_SIGHT),
					2*RANGE_OF_SIGHT, 2*RANGE_OF_SIGHT);
		}
		g.setColor(Color.BLACK);
		g.fillOval((int)(position.x-size/2-1), (int)(position.y-size/2-1), size+2, size+2);
		g.setColor(color);
		g.fillOval((int)(position.x-size/2), (int)(position.y-size/2), size, size);
	}

	/**
	 * Generates a new random direction for the amoeba.
	 */
	private void randomDirection() {
		double newX = speed.x + (Math.random()*SPEED_CHANGE - SPEED_CHANGE/2);
		double newY = speed.y + (Math.random()*SPEED_CHANGE - SPEED_CHANGE/2);
		
		
		Vector2d newVec = Utils.addVectors(speed, new Vector2d(newX, newY));
		if (newVec.length() > MAX_SPEED)
		{
			newVec.normalize();
			newVec.scale(MAX_SPEED);
		}
		
		speed = newVec;
		
		/*
		newX = GameUtils.clamp(newX, -MAX_SPEED, MAX_SPEED);
		newY = GameUtils.clamp(newY, -MAX_SPEED, MAX_SPEED);
		
		speed.x = newX;
		speed.y = newY;
		*/
	}
	
	/**
	 * Moves the amoeba towards the target variable.
	 */
	private void moveToTarget() {
		Vector2d direction = Utils.subVectors(target, position);
		direction.normalize();
		
		
		Vector2d newVec = Utils.addVectors(speed, Utils.scaleVector(direction, SPEED_CHANGE));
		if (newVec.length() > MAX_SPEED)
		{
			newVec.normalize();
			newVec.scale(MAX_SPEED);
		}
		
		speed = newVec;
		
		/*
		double newX = speed.x + direction.x*SPEED_CHANGE;
		double newY = speed.y + direction.y*SPEED_CHANGE;
		newX = GameUtils.clamp(newX, -MAX_SPEED, MAX_SPEED);
		newY = GameUtils.clamp(newY, -MAX_SPEED, MAX_SPEED);
		
		speed.x = newX;
		speed.y = newY;
		*/
	}

	/**
	 * The amoeba eats the given food.
	 * @param food The food that will be eaten.
	 */
	public void eatFood(Food food) {
		food.markForRemoval();
		
		size++;
		
		if (size >= SPAWNSIZE)
		{
			size -= NEWSIZE;
			simulation.spawnAmoeba(position, genome.getOffspring());
		}
	}
	
	/**
	 * Leaves a food object of the amoeba's color at the position of the amoeba.
	 */
	private void leaveExcrement()
	{
		timeSinceLastExcrement++;
		if (timeSinceLastExcrement > nextExcrement)
		{
			timeSinceLastExcrement = 0;
			nextExcrement = (int)(Math.random()*EXCR_SPREAD + (AVERAGE_EXCREMENT-EXCR_SPREAD/2.0));
			// Change here for energy loss
			if (Math.random() < 0.5)
			{
				simulation.spawnFood(position, color);
			}
			size--;
		}
	}
	
	/**
	 * Checks if the amoeba is dead.
	 */
	private void checkIfDead()
	{
		if (size <= STARVESIZE)
		{
			// Change here for energy loss.
			for (int i=0; i<(int)(size*0.5); i++)
			{
				simulation.spawnFood(position, color);
			}
			markForRemoval();
		}
	}
	
	/**
	 * Updates the state.
	 * @param closeFood A list of all food in close proximity.
	 */
	private void updateState(List<Food> closeFood) {
		lastStateUpdate++;
		
		if (lastStateUpdate >= updateStateInterval)
		{
			lastStateUpdate = 0;
			int index = foodInSight(closeFood);
			
			if (index != -1)
			{
				target = closeFood.get(index).getPosition();
				state = State.Eat;
			}
			else
			{
				state = State.Roam;
			}
		}
	}

	/**
	 * Moves the amoeba in different ways, according to the current state.
	 */
	private void move() {

		switch (state) {
		case Roam:
			randomDirection();
			break;
		case Eat:
			moveToTarget();
			break;

		default:
			break;
		}
	}

	/**
	 * If there is any food in sight, return the list index of that food.
	 * @param closeFood A list of all food in close proximity.
	 * @return -1 if there is no food in sight, else the list index of the food.
	 */
	private int foodInSight(List<Food> closeFood) {
		double closest = RANGE_OF_SIGHT+1;
		int closestIndex = -1;
		Food currentFood;
		double distance = 0;
		for (int i=0; i<closeFood.size(); i++)
		{
			currentFood = closeFood.get(i);
			if (!Utils.almostSameColor(currentFood.getColor(), color))
			{
				distance = Utils.distance(currentFood.getPosition(), position);
				if (distance < closest)
				{
					closest = distance;
					closestIndex = i;
				}
			}
		}
		return closestIndex;
	}
	
	/**
	 * Returns the information regarding the amoeba.
	 * @return Genetic information regarding the amoeba.
	 */
	public String getInfo()
	{
		return genome.toString();
	}
	
	/**
	 * Determines whether some coordinate collides with this amoeba.
	 * @param otherX The x component of the coordinate.
	 * @param otherY The y component of the coordinate.
	 * @return true if the coordinate collides with this amoeba, else false.
	 */
	public boolean collidesWith(double otherX, double otherY)
	{
		return collidesWith(new Vector2d(otherX, otherY));
	}
	
	/**
	 * Determines whether some coordinate collides with this amoeba.
	 * @param otherPos A vector containing components of the coordinate.
	 * @return true if the coordinate collides with this amoeba, else false.
	 */
	public boolean collidesWith(Vector2d otherPos)
	{
		return (Utils.distance(position, otherPos) < size/2.0);
	}
}
