package se.chalmers.pebjorn.life.model.objects;

import java.awt.Color;
import java.awt.Dimension;

import javax.vecmath.Vector2d;

/**
 * An abstract class representing a game object.
 * @author Plankton555
 * @version v0.3 (2012-02-19)
 */
public abstract class GameObject {

	protected Vector2d position;
	protected Vector2d speed;
	
	protected Color color;
	protected int size;
	
	private boolean removeThis = false;
	
	/**
	 * Creates a new game object with the default position (0,0).
	 */
	public GameObject()
	{
		this(new Vector2d());
	}
	
	/**
	 * Creates a new game object at the specified position.
	 * @param position The object's initial position.
	 */
	public GameObject(Vector2d position)
	{
		this.position = new Vector2d(position);
	}
	
	/**
	 * Returns the position.
	 * @return The position of the game object.
	 */
	public Vector2d getPosition()
	{
		return new Vector2d(position);
	}

	/**
	 * Makes sure that the object is not outside the provided dimension.
	 * @param clientBounds The dimension to check with.
	 */
	protected void keepInBounds(Dimension clientBounds) {
		
		position.x = (position.x > clientBounds.getWidth() ? clientBounds.getWidth()
				: (position.x < 0 ? 0 : position.x));
		position.y = (position.y > clientBounds.getHeight() ? clientBounds.getHeight()
				: (position.y < 0 ? 0 : position.y));
	}

	/**
	 * If the object is outside the given dimension, the speed will be "bounced".
	 * @param clientBounds The dimension to check with.
	 */
	protected void bounceOnWalls(Dimension clientBounds) {
		if (position.x < 0 || position.x > clientBounds.getWidth())
		{
			speed.x *= -1;
		}
		if (position.y < 0 || position.y > clientBounds.getHeight())
		{
			speed.y *= -1;
		}
	}
	
	/**
	 * Marks the object for removal.
	 */
	public void markForRemoval()
	{
		removeThis = true;
	}
	
	/**
	 * Indicates whether this object should be removed or not.
	 * @return true if this object should be removed.
	 */
	public boolean removeThis()
	{
		return removeThis;
	}
	
	/**
	 * @return The size of the game object.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @return The color of the game object.
	 */
	public Color getColor() {
		return color;
	}
}
