package se.chalmers.pebjorn.life.model;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;
import javax.vecmath.Vector2d;

import se.chalmers.pebjorn.bgf.AbstractGameView;
import se.chalmers.pebjorn.bgf.GameController;
import se.chalmers.pebjorn.bgf.IGame;
import se.chalmers.pebjorn.life.LifeUI;
import se.chalmers.pebjorn.life.LifeView;
import se.chalmers.pebjorn.life.model.objects.Amoeba;
import se.chalmers.pebjorn.life.model.objects.Food;

/**
 * A version of the Life project.
 * @author Plankton555
 * @version (2012-02-19)
 */
public class LifeSimulation implements IGame {
	
	private GameController gameController;
	private JPanel gamePanel;
	private LifeView gameView;
	
	private LifeUI ui;
	
	public static Random rand = new Random();;

	private final int INITIAL_FOOD = (int)((P.DIMENSION.getHeight()*
			P.DIMENSION.getWidth())/3000); //Gives one food per 3000 pixels
	
	private int totalTimePassed = 0;
	private int timeSinceLastFoodSpawn = 0;
	private int primProdTime = (int)(76000/(P.DIMENSION.getHeight()*
			P.DIMENSION.getWidth()/1000)); // Make this size independent
	//private int primProdTime = 50;
	
	private List<Amoeba> amoebas = new ArrayList<Amoeba>();
	private List<Food> foods = new ArrayList<Food>();
	
	private Color[] playerColors = { Color.RED, Color.BLUE, Color.YELLOW, Color.GREEN };
	
	// TODO Save/Load function
	
	// TODO Camera function (larger world, can move a camera in it)
	
	public LifeSimulation()
	{
		gamePanel = new JPanel();
		gameView = new LifeView();
		ui = new LifeUI(this);
		gamePanel.setLayout(new BorderLayout());
		gamePanel.add(gameView, BorderLayout.CENTER);
		gamePanel.add(ui, BorderLayout.EAST);
		
		gameView.addMouseListener(ui);
	}
	
	@Override
	public String getGameName() {
		return "Life v0.75";
	}

	@Override
	public void initialize() {
	
		gameController.pack();
		gameController.setLocationRelativeTo(null); //Centers the window
		
		spawnBasePopulation();
		
		for (int i=0; i<INITIAL_FOOD; i++)
		{
			spawnFood(getRandomPosition(), new Color(85, 107, 47));
			// Starts with only one color.
			
			//spawnFood(getRandomPosition(), playerColors[(i*playerColors.length)/INITIAL_FOOD]);
			// Splits the food in the different colors.
		}
	}

	@Override
	public void loadContent() {
		
	}

	@Override
	public void unloadContent() {
		
	}

	@Override
	public void update(Dimension clientBounds) {
		
		for (Amoeba amoeba : amoebas)
		{
			amoeba.update(clientBounds, foods);
		}
		for (Food food : foods)
		{
			food.update(clientBounds);
		}
		checkFoodCollisions();
		checkAllRemovals();
		
		if (amoebas.size() == 0)
		{
			spawnBasePopulation();
		}
		
		primaryProduction();

		totalTimePassed++;
		// TODO Continents (isolation, bottle-necks)
	}

	@Override
	public void draw(Graphics g, Dimension clientBounds) {
		for (Food food : foods)
		{
			food.draw(g);
		}
		for (Amoeba amoeba : amoebas)
		{
			amoeba.draw(g);
		}
		
		drawTime(g);
	}

	private void drawTime(Graphics g) {

		int sec = (int)(totalTimePassed/(1000/P.MS_PER_FRAME));
		int min = sec/60;
		sec = sec%60;
		int hour = min/60;
		min = min%60;
		g.setColor(Color.BLACK);
		g.drawString("Time: " + hour + "h " + min + "m " + sec + "s", 10, 20);
	}

	public void spawnAmoeba(Vector2d position, Genome genome)
	{
		amoebas.add(new Amoeba(position, genome, this));
	}
	
	public void spawnFood(Vector2d position, Color color)
	{
		foods.add(new Food(position, color));
	}
	
	private Vector2d getRandomPosition()
	{
		return new Vector2d(
				Math.random()*P.DIMENSION.width,
				Math.random()*P.DIMENSION.height);
	}
	
	private void checkFoodCollisions()
	{
		Amoeba currentAmoeba;
		for (int i=0; i<amoebas.size(); i++)
		{
			currentAmoeba = amoebas.get(i);
			for (Food food : foods)
			{
				if (!Utils.almostSameColor(currentAmoeba.getColor(), food.getColor()))
				{
					if (currentAmoeba.collidesWith(food.getPosition()))
					{
						amoebas.get(i).eatFood(food);
					}
				}
			}
		}
	}
	
	private void checkAllRemovals()
	{
		for (int i=0; i<foods.size(); i++)
		{
			if (foods.get(i).removeThis())
			{
				foods.remove(i);
				i--;
			}
		}
		for (int i=0; i<amoebas.size(); i++)
		{
			if (amoebas.get(i).removeThis())
			{
				amoebas.remove(i);
				i--;
			}
		}
	}
	
	private void spawnBasePopulation()
	{
		for (int i=0; i<playerColors.length; i++)
		{
			spawnAmoeba(getRandomPosition(), new Genome(playerColors[i]));
		}
	}
	
	private void primaryProduction()
	{
		timeSinceLastFoodSpawn++;
		if (timeSinceLastFoodSpawn > primProdTime)
		{
			timeSinceLastFoodSpawn = 0;
			spawnFood(getRandomPosition(), new Color(85, 107, 47));
		}
	}
	
	public String getAmoebaInfo(Point p)
	{
		for (Amoeba amoeba : amoebas)
		{
			if (amoeba.collidesWith(p.getX(), p.getY()))
			{
				return amoeba.getInfo();
			}
		}
		return "Nothing selected";
	}

	@Override
	public void setGameController(GameController controller) {
		this.gameController = controller;
	}

	public void togglePause() {
		gameController.togglePause();
	}

	public void setFastForward(int fastForward) {
		gameController.setFastForward(fastForward);
	}

	public void incrFastForward() {
		gameController.setFastForward(gameController.getFastForward()+1);
	}

	@Override
	public AbstractGameView getGameView()
	{
		return gameView;
	}

	@Override
	public JPanel getGamePanel()
	{
		return gamePanel;
	}
}
