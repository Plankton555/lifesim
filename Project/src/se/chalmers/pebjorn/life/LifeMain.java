package se.chalmers.pebjorn.life;

import se.chalmers.pebjorn.bgf.BasicGameMain;
import se.chalmers.pebjorn.life.model.LifeSimulation;
import se.chalmers.pebjorn.life.model.P;

public class LifeMain {

	public static void main(String[] args) {
		BasicGameMain.startGame(new LifeSimulation(), P.MS_PER_FRAME);
	}
}
