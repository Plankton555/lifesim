package se.chalmers.pebjorn.life;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.MatteBorder;

import se.chalmers.pebjorn.life.model.LifeSimulation;
import se.chalmers.pebjorn.life.model.P;

@SuppressWarnings("serial")
public class LifeUI extends JPanel implements ActionListener, MouseListener {
	
	private LifeSimulation game;
	
	private JTextArea infoField;
	
	private JSpinner speedSpinner;
	
	/**
	 * Creates the window.
	 * @param controller Reference to the game controller.
	 * @param view The game view.
	 * @param gameName Name of the game.
	 */
	public LifeUI(LifeSimulation game)
	{
		addButtons();
		
		this.game = game;
	}
	
	/**
	 * Creates the buttons.
	 * @return A JPanel containing the buttons.
	 */
	private void addButtons() {

		this.setLayout(new BorderLayout());
		JPanel upperButtPanel = new JPanel();
		JButton pauseButton = new JButton("Toggle pause");
		pauseButton.setActionCommand("pause");
		pauseButton.addActionListener(this);
		upperButtPanel.add(pauseButton);
		JButton debugButton = new JButton("Show debug");
		debugButton.setActionCommand("debug");
		debugButton.addActionListener(this);
		upperButtPanel.add(debugButton);
		
		// FFW function
		JPanel lowerButtPanel = new JPanel();
		speedSpinner = new JSpinner();
		speedSpinner.setPreferredSize(new Dimension(50, 25));
		speedSpinner.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		JButton setSpeedButton = new JButton("Set speed");
		setSpeedButton.setActionCommand("setSpeed");
		setSpeedButton.addActionListener(this);
		
		lowerButtPanel.add(speedSpinner);
		lowerButtPanel.add(setSpeedButton);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.add(upperButtPanel, BorderLayout.NORTH);
		buttonPanel.add(lowerButtPanel, BorderLayout.SOUTH);
		
		this.add(buttonPanel, BorderLayout.SOUTH);
		
		infoField = new JTextArea(20, 20);
		infoField.setEditable(false);
		this.add(infoField, BorderLayout.CENTER);
		
		this.setBorder(new MatteBorder(1, 1, 1, 1, Color.BLACK));
	}

	/**
	 * Handles the UI button presses.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String actionCommand = e.getActionCommand();
		
		if (actionCommand == "pause")
		{
			game.togglePause();
		}
		else if (actionCommand == "debug")
		{
			P.debug = !P.debug;
		}
		else if (actionCommand == "setSpeed")
		{
			game.setFastForward(((SpinnerNumberModel)speedSpinner.getModel()).getNumber().intValue());
		}
		
		// TODO Drag-select-box
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Object o = e.getSource();
		if (o instanceof LifeView)
		{
			Point position = e.getPoint();
			infoField.setText(game.getAmoebaInfo(position));
			
			//System.out.println(controller.getAmoebaInfo(position));
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

}
