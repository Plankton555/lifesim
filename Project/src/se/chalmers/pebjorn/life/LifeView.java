package se.chalmers.pebjorn.life;

import java.awt.Color;
import java.awt.Dimension;

import se.chalmers.pebjorn.bgf.AbstractGameView;
import se.chalmers.pebjorn.life.model.P;

@SuppressWarnings("serial")
public class LifeView extends AbstractGameView {

	public LifeView()
	{
		setBackground(new Color(102, 175, 170)); // Medium Aquamarine
		setPreferredSize(new Dimension(P.DIMENSION.width, P.DIMENSION.height));
	}
}
